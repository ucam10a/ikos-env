# export JAVA_HOME=
# export JRE_HOME=`dirname "$0"`/jre7
export BASE_IKOS=$PWD
export ANT_HOME=$BASE_IKOS/apache-ant-1.9.15
export HSQL_HOME=$BASE_IKOS/hsqldb
export TOMCAT_HOME=$BASE_IKOS/apache-tomcat-8.5.20
export PATH=$PATH:$ANT_HOME/bin
#echo $HSQL_HOME/data
cd $HSQL_HOME/data
gnome-terminal --tab -- ant
#echo $TOMCAT_HOME/bin
cd $TOMCAT_HOME/bin
sh startup.sh

